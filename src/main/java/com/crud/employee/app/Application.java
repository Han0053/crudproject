package com.crud.employee.app;

import com.crud.employee.crud.*;
import com.crud.employee.io.FileIO;

public class Application {
	
	public static void main( String[] args )
    {
       //crudByJdbc crud_jdbc = new crudByJdbc();
       crudByMybatis crud_mybatis = new crudByMybatis();
       FileIO io = new FileIO();
       
       //crud_jdbc.insert();
       //crud_jdbc.findById(50);
       //crud_jdbc.deleteById(71);
       //crud_jdbc.update(190, 80, 65);
       //crud_jdbc.batchInsert(io.readToList());
       //io.writeToFile(crud_jdbc.queryAll());
       
		//crud_mybatis.insert();
		//crud_mybatis.delete(1);
		//crud_mybatis.queryById(20);
		//crud_mybatis.updateById(1, 200, 80);
		//crud_mybatis.batchInsert(io.readToList());
		io.writeToFile(crud_mybatis.queryToList());
    }
	
}
