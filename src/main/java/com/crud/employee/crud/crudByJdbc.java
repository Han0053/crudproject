package com.crud.employee.crud;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.crud.employee.bean.Employee;

public class crudByJdbc {
	Scanner input = new Scanner(System.in);
	DecimalFormat df = new DecimalFormat("#####0.00");

	String url = "jdbc:mysql://localhost:3306/mysql?useTimezone=true&serverTimezone=GMT%2B8";
	String user = "root";
	String password = "gfHan0322";

	private static final String QUERY_EMP_BY_ID = "SELECT * FROM employee WHERE id = ";

	private static final String DELETE_EMP_by_ID = "DELETE FROM employee WHERE id = ";

	private static final String UPDATE_EMP = "UPDATE employee SET height = ?, weight = ? WHERE id = ?";

	private static final String INSERT_EMP = "INSERT INTO employee (id, height, weight, eng_Name, chi_Name, phone, email, bmi)"
			+ "VALUES(?, ?, ?, ?, ?, ?, ?, ?)";

	private static final String QUERY_ALL = "SELECT * FROM employee";

	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");

	public void insert() {

		try (Connection con = DriverManager.getConnection(url, user, password);
				PreparedStatement stmt = con.prepareStatement(INSERT_EMP)) {
//			stmt.setInt(1, emp.getId());
//			stmt.setInt(2, emp.getHeight());
//			stmt.setInt(3, emp.getWeight());
//			stmt.setString(4, emp.getEngName());
//			stmt.setString(5, emp.getChiName());
//			stmt.setString(6, emp.getPhone());
//			stmt.setString(7, emp.getEmail());
//			stmt.setDouble(8, emp.getBmi());

			stmt.setInt(1, 71);
			stmt.setInt(2, 180);
			stmt.setInt(3, 70);
			stmt.setString(4, "Harvey");
			stmt.setString(5, "鄭黃瀚");
			stmt.setString(6, "09123456789");
			stmt.setString(7, "han@gmail.com");
			stmt.setDouble(8, 20.7);
			stmt.executeUpdate();

			System.out.println("Insert success");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void findById(int selId) {

		try (Connection con = DriverManager.getConnection(url, user, password);
				Statement stmt = con.createStatement()) {

			ResultSet rs = stmt.executeQuery(QUERY_EMP_BY_ID + selId);

			while (rs.next()) {
				Integer id = rs.getInt("id");
				Integer height = rs.getInt("height");
				Integer weight = rs.getInt("weight");
				String engName = rs.getString("eng_Name");
				String chiName = rs.getString("chi_Name");
				String phone = rs.getString("phone");
				String email = rs.getString("email");
				Double bmi = rs.getDouble("bmi");
				String createTime = sdf.format(rs.getTimestamp("create_time"));
				String updateTime = sdf.format(rs.getTimestamp("update_time"));

				String output = "User %d: %d - %d - %s - %s - %s - %s - %f - %s - %s";
				System.out.println(String.format(output, id, height, weight, engName, chiName, phone, email, bmi,
						createTime, updateTime));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void deleteById(int delId) {

		try (Connection con = DriverManager.getConnection(url, user, password);
				PreparedStatement stmt = con.prepareStatement(DELETE_EMP_by_ID + delId)) {

			stmt.executeUpdate();
			System.out.println("Delete success");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void update(int upHei, int upWei, int id) {

		try (Connection con = DriverManager.getConnection(url, user, password);
				PreparedStatement stmt = con.prepareStatement(UPDATE_EMP)) {

			stmt.setInt(1, upHei);
			stmt.setInt(2, upWei);
			stmt.setInt(3, id);
			stmt.executeUpdate();

			System.out.println("Update success");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void batchInsert(List<Employee> empList) {

		Connection con = null;
		PreparedStatement stmt = null;

		try {
			con = DriverManager.getConnection(url, user, password);
			stmt = con.prepareStatement(INSERT_EMP);
			con.setAutoCommit(false);

			for (Employee emp : empList) {
				stmt.setInt(1, emp.getId());
				stmt.setInt(2, emp.getHeight());
				stmt.setInt(3, emp.getWeight());
				stmt.setString(4, emp.getEngName());
				stmt.setString(5, emp.getChiName());
				stmt.setString(6, emp.getPhone());
				stmt.setString(7, emp.getEmail());
				stmt.setDouble(8, emp.getBmi());
				
				System.out.println(emp.getId());

				stmt.addBatch();
			}

			stmt.executeBatch();
			con.commit();
			System.out.println("Batch insert success");
		} catch (SQLException e) {
			try {
				con.rollback();
				System.out.println("Data Error");
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public List<Employee> queryAll() {

		List<Employee> empList = new ArrayList<Employee>();

		try (Connection con = DriverManager.getConnection(url, user, password);
				Statement stmt = con.createStatement()) {

			ResultSet resultSet = stmt.executeQuery(QUERY_ALL);
			while (resultSet.next()) {

				Employee emp = new Employee();

				emp.setId(resultSet.getInt("id"));
				emp.setHeight(resultSet.getInt("height"));
				emp.setWeight(resultSet.getInt("weight"));
				emp.setEngName(resultSet.getString("eng_Name"));
				emp.setChiName(resultSet.getString("chi_Name"));
				emp.setPhone(resultSet.getString("phone"));
				emp.setEmail(resultSet.getString("email"));
				emp.setBmi(resultSet.getDouble("bmi"));
				emp.setCreateTime(resultSet.getTimestamp("create_time"));
				emp.setUpdateTime(resultSet.getTimestamp("update_time"));

				empList.add(emp);
			}
			System.out.println("Query all success");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return empList;
	}
}