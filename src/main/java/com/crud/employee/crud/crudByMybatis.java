package com.crud.employee.crud;

import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.crud.employee.bean.Employee;
import com.crud.employee.oper.EmpOperation;

public class crudByMybatis {
	
	private static SqlSessionFactory sqlSessionFactory;
	private static Reader reader;

	static {
		try {
			reader = Resources.getResourceAsReader("config.xml");
			sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static SqlSessionFactory getSession() {
		return sqlSessionFactory;
	}

	public void queryById(int id) {

		SqlSession session = sqlSessionFactory.openSession();

		try {
			EmpOperation empOper = session.getMapper(EmpOperation.class);
			Employee emp = empOper.queryById(id);

			String output = "User %d: %d - %d - %s - %s - %s - %s - %f - %s - %s";
			System.out.println(
					String.format(output, id, emp.getHeight(), emp.getWeight(), emp.getEngName(), emp.getChiName(),
							emp.getPhone(), emp.getEmail(), emp.getBmi(), emp.getCreateTime(), emp.getUpdateTime()));
		} finally {
			session.close();
		}
	}

	public void delete(int id) {

		SqlSession session = sqlSessionFactory.openSession();

		try {
			EmpOperation empOper = session.getMapper(EmpOperation.class);
			empOper.deleteById(id);

			session.commit();
			System.out.print("delete success");
		} finally {
			session.close();
		}
	}

	public void insert() {

		SqlSession session = sqlSessionFactory.openSession();

		try {
			EmpOperation empOper = session.getMapper(EmpOperation.class);
			Employee emp = new Employee();

			emp.setId(1);
			emp.setHeight(180);
			emp.setWeight(70);
			emp.setEngName("hank");
			emp.setChiName("鄭黃瀚");
			emp.setPhone("1222");
			emp.setEmail("hank@gmail.com");
			emp.setBmi(20.2);

			empOper.insert(emp);
			session.commit();
			System.out.print("insert success");
		} finally {
			session.close();
		}
	}

	public void updateById(int id, int hei, int wei) {

		SqlSession session = sqlSessionFactory.openSession();

		try {
			EmpOperation empOper = session.getMapper(EmpOperation.class);
			Employee emp = empOper.queryById(id);

			emp.setHeight(hei);
			emp.setWeight(wei);

			empOper.updateById(emp);
			session.commit();
		} finally {
			session.close();
		}
	}

	public void batchInsert(List<Employee> empList) {

		SqlSession session = sqlSessionFactory.openSession();

		try {
			EmpOperation empOper = session.getMapper(EmpOperation.class);

			empOper.batchInsert(empList);

			session.commit();
		} catch (Exception e) {
			session.rollback();
		} finally {
			session.close();
		}
	}

	public List<Employee> queryToList() {

		SqlSession session = sqlSessionFactory.openSession();
		List<Employee> empList = new ArrayList<Employee>();

		try {
			EmpOperation empOper = session.getMapper(EmpOperation.class);
			empList = empOper.download();
			// StringBuilder sb = new StringBuilder();

//			for (Employee emp : empList) {
//				sb.append(emp.getId()).append(",");
//				sb.append(emp.getHeight()).append(",");
//				sb.append(emp.getWeight()).append(",");
//				sb.append(emp.getEngName()).append(",");
//				sb.append(emp.getChiName()).append(",");
//				sb.append(emp.getPhone()).append(",");
//				sb.append(emp.getEmail()).append(",");
//				sb.append(emp.getBmi()).append(",");
//				sb.append(emp.getCreateTime()).append(",");
//				sb.append(emp.getUpdateTime()).append("\t");
//			}
		} finally {
			session.close();
		}
		return empList;
	}
}
