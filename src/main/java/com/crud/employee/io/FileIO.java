package com.crud.employee.io;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import com.crud.employee.bean.Employee;

public class FileIO {
	public List<Employee> readToList() {

		List<String> strList = new ArrayList<String>();
		List<Employee> empList = new ArrayList<Employee>();

		try (BufferedReader read = new BufferedReader(new InputStreamReader(
				new FileInputStream("D:\\Spring Boot Workspace\\CsvData\\employee.csv"), "UTF-8"))) {

			String line;
			while ((line = read.readLine()) != null) {
				strList.add(line);
			}

			for (String str : strList) {
				String[] tmpAry = str.split(",");

				Employee emp = new Employee();

				emp.setId(Integer.parseInt(tmpAry[0]));
				emp.setHeight(Integer.parseInt(tmpAry[1]));
				emp.setWeight(Integer.parseInt(tmpAry[2]));
				emp.setEngName(tmpAry[3]);
				emp.setChiName(tmpAry[4]);
				emp.setPhone(tmpAry[5]);
				emp.setEmail(tmpAry[6]);
				emp.setBmi(Double.parseDouble(tmpAry[7]));

				empList.add(emp);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return empList;
	}

	public void writeToFile(List<Employee> empList) {
		try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream("D://Download//downloadEmp.csv"),
				StandardCharsets.UTF_8)) {

			StringBuilder sb = new StringBuilder();
			for (Employee emp : empList) {
				sb.append(emp.getId()).append(",");
				sb.append(emp.getHeight()).append(",");
				sb.append(emp.getWeight()).append(",");
				sb.append(emp.getEngName()).append(",");
				sb.append(emp.getChiName()).append(",");
				sb.append(emp.getPhone()).append(",");
				sb.append(emp.getEmail()).append(",");
				sb.append(emp.getBmi()).append(",");
				sb.append(emp.getCreateTime()).append(",");
				sb.append(emp.getUpdateTime()).append("\n");
			}

			writer.write(sb.toString());
			System.out.println("Write to file success");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
