package com.crud.employee.oper;

import java.util.List;

import com.crud.employee.bean.Employee;

public interface EmpOperation {
	 Employee queryById(int id);

	 void insert(Employee emp);

	 void updateById(Employee emp);

	 void deleteById(int id);

	 void batchInsert(List<Employee> list);

	 List<Employee> download();
}
